#include <cstdio>
#include <iostream>
#include "UtilityFunctions.h"
#include "LocationData.h"
#include "NearestNeighbour.h"
#include <cmath>
#include <cstdlib>


const int TOTAL_TESTS = 10;


/* Output of tests in Test Anything Protocol (TAP) format
 * e.g:
 * 1..2
 * not ok 1 - first test
 * ok 2 - second test
 */ 
int main()
{
	printf("1..%i\n", TOTAL_TESTS);
	int test_num = 1;

	// Test 1: Different values for Lat/Long
	{
		int lat_degrees_a = 49, lat_minutes_a = 13,
			lon_degrees_a = 123, lon_minutes_a = 6;
		int lat_degrees_b = 51, lat_minutes_b = 1,
			lon_degrees_b = 114, lon_minutes_b = 1;
		long expected = 678;
		long actual = std::lround(distance_between_lat_lon(
			lat_degrees_a, lat_minutes_a,
			lon_degrees_a, lon_minutes_a,
			lat_degrees_b, lat_minutes_b,
			lon_degrees_b, lon_minutes_b
		));
		bool success = expected == actual;
		printf("%sok %i - Testing distance_between_lat_lon()(different locations)\n", success ? "" : "not ", test_num++);
	}
	// Test 2: Same values for Lat/Long
	{
		int lat_degrees = 49, lat_minutes = 13,
			lon_degrees = 123, lon_minutes = 6;
		long actual = std::lround(distance_between_lat_lon(
			lat_degrees, lat_minutes,
			lon_degrees, lon_minutes,
			lat_degrees, lat_minutes,
			lon_degrees, lon_minutes
		));
		bool success = 0 == actual;
		printf("%sok %i - Testing distance_between_lat_lon()(same location)\n", success ? "" : "not ", test_num++);
	}
	// Test 3: FileParser correct input (unix lines)
	{
		const std::vector<std::vector<std::string>> expected = {
			{ "Vancouver","49","13","123","06" },
			{ "Calgary","51","1","114","1" },
			{ "Edmonton","53","34","113","28" }
		};
		LocationData data;
		data.load_from_file("test_resources/test_0.txt");
		bool success = data.get_data() == expected;
		printf("%sok %i - Load location data from test_0.txt (unix line endings)\n", success ? "" : "not ", test_num++);
	}
	// Test 4: FileParser correct input (dos lines)
	{
		const std::vector<std::vector<std::string>> expected = {
			{ "Vancouver","49","13","123","06" },
			{ "Calgary","51","1","114","1" },
			{ "Edmonton","53","34","113","28" }
		};
		LocationData data;
		data.load_from_file("test_resources/test_1.txt");
		bool success = data.get_data() == expected;
		printf("%sok %i - Load location data from test_1.txt (dos line endings)\n", success ? "" : "not ", test_num++);
	}
	// Test 5: FileParser empty file 
	{
		const std::vector<std::vector<std::string>> expected = {};
		LocationData data;
		data.load_from_file("test_resources/test_2.txt");
		bool success = data.get_data() == expected;
		printf("%sok %i - Load location data from test_2.txt (empty file)\n", success ? "" : "not ", test_num++);
	}
	// Test 6: FileParser bad formatted file 
	{
		const std::vector<std::vector<std::string>> expected = {};
		LocationData data;
		bool result = data.load_from_file("test_resources/test_3.txt");
		bool success = result == false;
		printf("%sok %i - Load location data from test_3.txt (incorrect format)\n", success ? "" : "not ", test_num++);
	}
	// Test 7: Test algorithm, 3 locations 
	{
		TSP::NearestNeighbourAlgorithm algo;
		std::vector<std::vector<std::string>> data = {
			{ "Vancouver","49","13","123","06" },
			{ "Calgary","51","1","114","1" },
			{ "Edmonton","53","34","113","28" }
		};
		for (auto location : data){
			algo.add_location(location[0], 
				std::stoi(location[1]), std::stoi(location[2]),
				std::stoi(location[3]), std::stoi(location[4])
			);
		}
		TSP::Result results = algo.execute("Vancouver");
		std::vector<std::string> expected_route = { "Vancouver", "Calgary", "Edmonton" };
		int expected_distance =	std::lround(distance_between_lat_lon(
				std::stoi(data[0][1]), std::stoi(data[0][2]),
				std::stoi(data[0][3]), std::stoi(data[0][4]),
				std::stoi(data[1][1]), std::stoi(data[1][2]),
				std::stoi(data[1][3]), std::stoi(data[1][4])
			)	+ distance_between_lat_lon(
				std::stoi(data[1][1]), std::stoi(data[1][2]),
				std::stoi(data[1][3]), std::stoi(data[1][4]),
				std::stoi(data[2][1]), std::stoi(data[2][2]),
				std::stoi(data[2][3]), std::stoi(data[2][4])
			)
		);
		bool success = expected_distance == results.get_total_distance() && expected_route == results.get_route();
		printf("%sok %i - Test algorithm on simple case (3 locations)\n", success ? "" : "not ", test_num++);
	}
	// Test 8: Test algorithm, 1 location
	{
		TSP::NearestNeighbourAlgorithm algo;
		std::vector<std::vector<std::string>> data = {
			{ "Vancouver","49","13","123","06" }
		};
		for (auto location : data){
			algo.add_location(location[0], 
				std::stoi(location[1]), std::stoi(location[2]),
				std::stoi(location[3]), std::stoi(location[4])
			);
		}
		TSP::Result results = algo.execute("Vancouver");
		std::vector<std::string> expected_route = { "Vancouver" };
		bool success = (0 == results.get_total_distance() && expected_route == results.get_route());
		printf("%sok %i - Test algorithm on simple case (1 location)\n", success ? "" : "not ", test_num++);
	}
	// Test 9: Test algorithm complex case (127 cities)
	{
		TSP::NearestNeighbourAlgorithm algo(10000); // Reduce iterations for the test
		std::vector<std::vector<std::string>> data;
		srand(42);
		for (int i = 0; i < 127; ++i){ 
			data.push_back({ std::to_string(i)+"City",std::to_string(rand()%50),std::to_string(rand()%50),std::to_string(rand()%50),std::to_string(rand()%50)});
		}
		for (auto location : data){
			algo.add_location(location[0], 
				std::stoi(location[1]), std::stoi(location[2]),
				std::stoi(location[3]), std::stoi(location[4])
			);
		}
		TSP::Result results = algo.execute("0City");
		bool success = results.get_route().size() == 127;
		printf("%sok %i - Test algorithm on complex case (127 locations)\n", success ? "" : "not ", test_num++);
	}
	// Test 10: Test algorithm, 0 locations 
	{
		TSP::NearestNeighbourAlgorithm algo;
		TSP::Result results = algo.execute("Vancouver");
		bool success = (0 == results.get_total_distance() && results.get_route().empty());
		printf("%sok %i - Test algorithm with no locations\n", success ? "" : "not ", test_num++);
	}

	return 0;
}

