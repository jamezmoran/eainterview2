#pragma once

#include <vector>
#include <string>

class LocationData
{
	using TableData = std::vector<std::vector<std::string>>;
	public:
		LocationData();

		bool load_from_file(const std::string& filename);

		const TableData& get_data() const;
	
	private:
		TableData m_data;
};
