#pragma once

#include <queue>
#include <vector>
#include <string>
#include <functional>
#include <unordered_map>
#include <unordered_set>

namespace TSP {
	class Result final
	{
		public:
			Result(const std::vector<std::string> route, int total_distance);
			const std::vector<std::string>& get_route() const;
			int get_total_distance() const;
		private:
			const std::vector<std::string> m_route;
			const int m_total_distance;
	};

	class NearestNeighbourAlgorithm final
	{
		using MinHeapEntry = std::pair<int, std::string>;
		using MinHeap = std::priority_queue<MinHeapEntry>;
		public:
			NearestNeighbourAlgorithm(int max_searches = 100000);

			Result execute(const std::string& start_location);
			void add_location(
				const std::string& name,
				int lat, int lat_minutes,
				int lon, int lon_minutes
			);

		private:
			Result find_shortest_route(
				const std::string &start, 
				const std::unordered_map<std::string, MinHeap>& neighbours
			) const;

			const int m_max_searches;
			struct Location
			{
				Location(
					const std::string& name, 
					int lat, int lat_minutes, 
					int lon, int lon_minutes
				);
				std::string m_name;
				int m_lat, m_lat_minutes;
				int m_lon, m_lon_minutes;
			};
			std::vector<Location> m_locations;
	};
}
