#include <fstream>
#include <iostream>
#include "LocationData.h"
#include "UtilityFunctions.h"

LocationData::LocationData(){}

bool LocationData::load_from_file(const std::string &filename){
	m_data.clear();
	std::ifstream infile(filename);
	std::string line;
	while (std::getline(infile, line)){
		if (line.length() <= 1)
			continue;
		std::vector<std::string> record = split_delimited_string(line, ",");
		if (record.size() != 5){
			return false;
		}
		// Get rid of line endings
		for (auto& item : record){
			item = item.substr(item.find_first_not_of(" "));
			for (int i = item.length()-1; i >= 0; --i){
				if (item[i] == '\n' || item[i] == '\r'){
					item.pop_back();
				} else {
					break;
				}
			}
		}
		m_data.push_back(std::move(record));
	}
	return true;
}

const LocationData::TableData& LocationData::get_data() const {
	return m_data;
}
