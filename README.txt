
ConcertScheduling Challenge - James Moran
---------------------------

Below are some comments on my thought process for the design of this solution.
This is a classic case of the travelling sales person problem. Because of the
combinatorial nature of this problem, creating an algorithm to find the most
optimal solution is out of the question. Therefore, we need to at least find a
sub-optimal solution within the time constraints.

To do this, I've decided to create an A* depth-first search algorithm which
uses each city's nearest neighbours as a heuristic for traversing the solution
space. It first greedily traverses down the subtrees with the shortest
distance, and then once it comes to a solution, it attempts to find a better
solution by working back up the stack and testing other similar solutions, as
well as throwing out any routes early that are already worse than our best
solution so far. By default, the command line program only searches through
100,000 possible solutions, but this can be adjusted using a command line
parameter at the expense of a longer execution time.

How to build and run
--------------------

I've provided several different methods for building this program, depending
on the platform. On linux, I have provided a Makefile that will build both the
command line program and the test driver that I have created. Below is example
usage:

$ make all
$ ./ConcertScheduling test_resources/test_full.txt
Vancouver
Edmonton
..
4813km
$ ./ConcertSchedulingTests
1..10
ok 1 - Testing distance_between_lat_lon()(different locations)
ok 2 - Testing distance_between_lat_lon()(same location)
..

For Windows, there are visual studio 2017 projects in the vc15 folder that
should be able to build these source files. There is also a CMakeLists.txt as
well if the target build platform also has CMake installed.

Thank you for taking the time to review this submission. I look forward to
hearing back.

James Moran
