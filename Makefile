CC=g++
CFLAGS=-c -Wall

all: main test

main: main.o NearestNeighbour.o UtilityFunctions.o LocationData.o
	$(CC) main.o NearestNeighbour.o UtilityFunctions.o LocationData.o -o ConcertScheduling

test: test.o NearestNeighbour.o UtilityFunctions.o LocationData.o
	$(CC) TestDriver.o NearestNeighbour.o UtilityFunctions.o LocationData.o -o ConcertSchedulingTests

main.o: main.cpp
	$(CC) $(CFLAGS) main.cpp

NearestNeighbour.o: NearestNeighbour.cpp
	$(CC) $(CFLAGS) NearestNeighbour.cpp

UtilityFunctions.o:
	$(CC) $(CFLAGS) UtilityFunctions.cpp 

LocationData.o: LocationData.cpp
	$(CC) $(CFLAGS) LocationData.cpp

test.o: TestDriver.cpp
	$(CC) $(CFLAGS) TestDriver.cpp

clean:
	rm *o ConcertScheduling ConcertSchedulingTests
