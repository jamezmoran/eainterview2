#include <iostream>
#include <memory>
#include "NearestNeighbour.h"
#include "LocationData.h"

using namespace TSP;

int main(int argc, char** argv){
	if (argc < 2){
		std::cout << "Usage: ./ConcertScheduling <filename> [max_searches=100000]" << std::endl;
	}
	const int max_searches = argc == 3 ? std::stoi(argv[2]) : 100000;

	// Create pointer that can hold any type of TSP Algorithm,
	// select based on cmdline argument #2.
	NearestNeighbourAlgorithm algo(max_searches);
	LocationData data;
	data.load_from_file(argv[1]);
	for (const auto& record : data.get_data()){
		try {
			algo.add_location(record[0], 
				std::stoi(record[1]), std::stoi(record[2]),
				std::stoi(record[3]), std::stoi(record[4])
			);
		} catch (const std::invalid_argument& e){
			std::cerr << "Detected error in the format of locations" << std::endl;
			continue;
		}
	}
	
	// Execute
	Result result = algo.execute("Vancouver");

	// Print the route and total distance covered.
	for (const std::string& name : result.get_route()){
		std::cout << name << std::endl;
	}
	std::cout << result.get_total_distance()  << " km" << std::endl;
	return 0;
}
