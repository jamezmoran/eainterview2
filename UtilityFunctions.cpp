#include "UtilityFunctions.h"
#ifdef _WIN32
#define M_PI 3.14159265358979323846264338327950288419716939937510
#endif
#include <cmath>

double degrees_to_radians(double degrees){
	return degrees * M_PI / 180;
}

double distance_between_lat_lon(
	double lat_a, double lat_minutes_a, double lon_a, double lon_minutes_a, 
	double lat_b, double lat_minutes_b, double lon_b, double lon_minutes_b
){
	static const double EARTH_RADIUS = 6373;
	double lat_radians_a = degrees_to_radians(lat_a + (lat_minutes_a/60));
	double lon_radians_a = degrees_to_radians(lon_a + (lon_minutes_a/60));
	double lat_radians_b = degrees_to_radians(lat_b + (lat_minutes_b/60));
	double lon_radians_b = degrees_to_radians(lon_b + (lon_minutes_b/60));
	double delta_lat = lat_radians_b - lat_radians_a;
	double delta_lon = lon_radians_b - lon_radians_a;
	double sin_delta_lat = sin(delta_lat/2); // For optimization
	double sin_delta_lon = sin(delta_lon/2); //
	double a = (sin_delta_lat*sin_delta_lat) 
		+ cos(lat_radians_a)
		* cos(lat_radians_b)
		* (sin_delta_lon*sin_delta_lon);
	double c = 2 * atan2(sqrt(a), sqrt(1-a));
	return EARTH_RADIUS * c;
}
std::vector<std::string> split_delimited_string(const std::string& input, const std::string& delim){
		std::vector<std::string> split;
		int start = 0;
		for (size_t pos = input.find(delim); pos != std::string::npos; pos = input.find(delim, start)){
			split.push_back(input.substr(start, pos-start));
			start = pos+1;
		}
		split.push_back(input.substr(start));
		return std::move(split);
}
