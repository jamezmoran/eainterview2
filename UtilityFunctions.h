#pragma once
#include <vector>
#include <string>

double distance_between_lat_lon(
	double lat_a, double lat_minutes_a, double lon_a, double lon_minutes_a,
	double lat_b, double lat_minutes_b, double lon_b, double lon_minutes_b
);

std::vector<std::string> split_delimited_string(const std::string& input, const std::string& delim);
