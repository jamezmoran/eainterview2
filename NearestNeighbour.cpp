#include <cmath>
#include <limits>
#include <stack>
#include "NearestNeighbour.h"
#include "UtilityFunctions.h"

namespace TSP {
	NearestNeighbourAlgorithm::NearestNeighbourAlgorithm(int max_searches):
		m_max_searches(max_searches)
	{
	}
	void NearestNeighbourAlgorithm::add_location(const std::string& name, int lat, int lat_minutes, int lon, int lon_minutes){
		m_locations.push_back(Location(name, lat, lat_minutes, lon, lon_minutes));
	}

	NearestNeighbourAlgorithm::Location::Location(const std::string& name, int lat, int lat_minutes, int lon, int lon_minutes):
		m_name(name),
		m_lat(lat),
		m_lat_minutes(lat_minutes),
		m_lon(lon),
		m_lon_minutes(lon_minutes)
	{
	}

	Result::Result(const std::vector<std::string> route, int total_distance):
		m_route(route),
		m_total_distance(total_distance)
	{
	}

	const std::vector<std::string>& Result::get_route() const {
		return m_route;
	}
	int Result::get_total_distance() const {
		return m_total_distance;
	}

	// Depth-first search using nearest neighbour as heuristic.
	// Do iteratively to avoid stack overflow.
	Result NearestNeighbourAlgorithm::find_shortest_route(
		const std::string &start, 
		const std::unordered_map<std::string, MinHeap>& neighbours
	) const {
		// Initialize final route and distance
		std::vector<std::string> final_route;
		int final_distance = std::numeric_limits<int>::max();

		// Initialize variables that will be modified as the search progresses.
		// i.e visited nodes, stack, current route and distance
		std::unordered_set<std::string> visited;
		std::stack<std::tuple<std::string, int, bool>> search_stack;
		std::vector<std::string> current_route = { };

		// Push starting node to stack
		search_stack.push({start, 0, false});
		int current_searches = 0;
		while(!search_stack.empty()){
			std::string current_location = std::get<0>(search_stack.top());
			int distance_travelled = std::get<1>(search_stack.top());
			bool &evaluated = std::get<2>(search_stack.top());

			// For a stack frame that we've already read, undo the changes that were
			// made by it to the current route and nodes we've visited.
			if (evaluated){
				search_stack.pop();
				current_route.pop_back();
				visited.erase(current_location);
				continue;
			} else {
				evaluated = true;
			}

			// Add distance to travel to current distance, add route
			current_route.push_back(current_location);
			visited.insert(current_location);

			// Quit early if this branch is already worse than our current best.
			if (distance_travelled >= final_distance){
				if (++current_searches >= m_max_searches){
					break;
				}
				continue;
			}

			// If we have visited all nodes
			if (visited.size() == m_locations.size()){
				// Update final route if the current is shorter.
				if (distance_travelled < final_distance){
					final_route = current_route;
					final_distance = distance_travelled;
				}
				if (++current_searches >= m_max_searches){
					break;
				}
			} else {
				// Iterate over heap of nearest neighbours and add all unvisited children to the stack
				for (MinHeap current_location_neighbours = neighbours.at(current_location); current_location_neighbours.size() > 0; current_location_neighbours.pop()){
					int distance_to_travel = current_location_neighbours.top().first;
					std::string next_location = current_location_neighbours.top().second;
					if (visited.count(next_location) == 0){
						search_stack.push({next_location, distance_travelled + distance_to_travel, false});
					}
				}
			}
		}
		return Result(final_route, final_distance);
	}

	Result NearestNeighbourAlgorithm::execute(const std::string& start_location)
	{
		if (m_locations.size() < 1){
			return Result({}, 0);
		} else if (m_locations.size() == 1){
			return Result({ m_locations[0].m_name }, 0);
		}

		// Find the nearest neighbours of all locations
		std::unordered_map<std::string, MinHeap> neighbour_heaps;
		for (const auto& location_1 : m_locations){
			for (const auto& location_2 : m_locations){
				if (location_1.m_name == location_2.m_name)
					continue;
				auto &heap = neighbour_heaps[location_1.m_name];
				heap.push({
					std::lround(
						distance_between_lat_lon(
							location_1.m_lat, location_1.m_lat_minutes, location_1.m_lon, location_1.m_lon_minutes,
							location_2.m_lat, location_2.m_lat_minutes, location_2.m_lon, location_2.m_lon_minutes
						)
					),
					location_2.m_name
				});
			}
		}
		return find_shortest_route(start_location, neighbour_heaps);
	}
}
